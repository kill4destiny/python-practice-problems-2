# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    #create a list var of the values
    my_list = [value1, value2, value3]
    #create a max variable with the value of zero
    max = 0
    #iterate over the list
    for item in my_list:
        # if max is less than or equal to item in list:
        if max <= item:
            #max equals item on the list
            max = item
    #return max
    return max


print(max_of_three(1, 2, 3))
print(max_of_three(5, 8, 10))
print(max_of_three(10, 3, 5))
print(max_of_three(5, 8, 8))
