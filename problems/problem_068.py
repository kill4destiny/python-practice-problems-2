# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list


# class Person
class Person:
    # method initializer with name, hated foods list, and loved foods list
    def __init__(self, name, loved_food, hated_food):
        # self.name = name
        self.name = name
        # self.hated_foods = hated_foods
        self.loved_food = loved_food
        # self.loved_foods = loved_foods
        self.hated_food = hated_food
    # method taste(self, food)
    def taste(self, food):
        # if food is in self.hated_foods
        if food in self.loved_food:
            # return False
            return True
        # otherwise, if food is in self.loved_foods
        elif food in self.hated_food:
            # return True
            return False
        # otherwise
        else:
            # return None
            return None


person = Person('Luiz', ['tacos', 'tortas', 'quesadillas'], 
                ['champurrados', 'white beans', 'brussel sprouts'])
print(person.taste('lasagna'))
print(person.taste('tacos'))
print(person.taste('tortas'))
print(person.taste('champurrados'))
