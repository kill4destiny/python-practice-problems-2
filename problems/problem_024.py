# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    #if len(values) == 0:
    if len(values) == 0:
        #return None
        return None
    #return sum of list divided by length of list
    return sum(values) / len(values)


print(calculate_average([10])) #10
print(calculate_average([10, 20])) #15
print(calculate_average([5, 10, 15, 20, 50])) #20
print(calculate_average([])) # None
