# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(num):
    result = ''
    if num == 1:
        result = 'I'
    elif num == 2:
        result = 'II'
    elif num == 3:
        result = 'III'
    elif num == 4:
        result = 'IV'
    elif num == 5:
        result = 'V'
    elif num == 6:
        result = 'VI'
    elif num == 7:
        result = 'VII'
    elif num == 8:
        result = 'VIII'
    elif num == 9:
        result = 'IX'
    elif num == 10:
        result = 'X'
    return result

print(simple_roman(4))
print(simple_roman(3))
print(simple_roman(2))
