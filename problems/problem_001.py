# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    # if value1 is greater than or equal to value2:
    if value1 >= value2:
        # return value1
        return value1
    # elif value2 is greater than or equal to value 1:
    elif value2 >= value1:
        # return value2
        return value2


print(minimum_value(22, 8))
print(minimum_value(10, 10))
print(minimum_value(12, 22))
print(minimum_value(10, 11))
