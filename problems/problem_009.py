# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #create a variable with the value being the word reversed
    reverse_word = word[:: -1]
    #if word equals reverse_word:
    if word == reverse_word:
        #return 'Is Palindrome!'
        return 'Is Palindrome!'
    #else:
    else:
        #return 'It is not a Palindrome!'
        return 'It is not a Palindrome!'


print(is_palindrome('racecar'))
print(is_palindrome('wow'))
print(is_palindrome('nuts'))
