# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(str):
    lists = []
    result = []
    for i in str:
        if i == 'z':
            i = '`'
        elif i == 'Z':
            i = '@'
        nums = ord(i) + 1
        lists.append(nums)
        for j in lists:
            back_to_char = chr(j)
        result.append(back_to_char)
    return ''.join(result)


print(shift_letters('import'))
print(shift_letters('ABBA'))
print(shift_letters('Kala'))
print(shift_letters('zap'))
print(shift_letters('Zap'))