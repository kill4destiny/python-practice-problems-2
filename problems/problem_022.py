# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    my_list = []
    if is_workday and is_sunny:
        my_list.append('laptop')
    if is_workday is False and is_sunny:
        my_list.append('surfboard')
    if is_workday and is_sunny is False:
        my_list.append('umbrella')
    return my_list


print(gear_for_day(False, True))
print(gear_for_day(True, True))
print(gear_for_day(True, False))
