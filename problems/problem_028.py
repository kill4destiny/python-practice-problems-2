# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the *String is empty, then return the empty string.

def remove_duplicate_letters(s):
    #edgecase
    #if string empty:
    if len(s) == 0:
        #return ''
        return ''
    #variable my_str = ''
    my_str = ''
    #for letter in s:
    for letter in s:
        #if letter not in my_str:
        if letter not in my_str:
            #my_str += letter
            my_str += letter
    #return my_str
    return my_str


print(remove_duplicate_letters('abc'))
print(remove_duplicate_letters('abcabc'))
print(remove_duplicate_letters('abccba'))
print(remove_duplicate_letters('abccbad'))
