# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(member_list, attendees_list):
    halfed = len(member_list) / 2
    if len(attendees_list) >= halfed:
        return True
    else:
        return False


print(has_quorum(["Basia", "Noor", "Laila", "Talia", "Zahara"],
      ["Basia", "Noor", "Laila", "Zahara"])) #True 80%
print(has_quorum(["Basia", "Noor", "Laila", "Talia", "Zahara"], 
      ["Noor", "Laila", "Zahara"])) #True 60%
print(has_quorum(["Basia", "Noor", "Laila", "Talia", "Zahara"],
      ["Basia", "Noor"])) #False 40%
