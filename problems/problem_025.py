# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    #edgecase
    #if len(values) == 0:
    if len(values) == 0:
        #return None
        return None
    #return sum(values)
    return sum(values)


print(calculate_sum([2, 34, 2, 3]))
print(calculate_sum([1, 2, 4, 5]))
print(calculate_sum([5, 5, 5, 5]))
