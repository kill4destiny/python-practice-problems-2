# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

def remove_duplicates(list):
    #variable my_list = []
    my_list = []
    #for num in list:
    for num in list:
        #if num not in my_list:
        if num not in my_list:
            #my_list.append(num)
            my_list.append(num)
    #return my_list
    return sorted(my_list)


print(remove_duplicates([1, 1, 1, 1]))
print(remove_duplicates([1, 2, 2, 1]))
print(remove_duplicates([1, 3, 3, 20, 3, 2, 2]))
