# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class Person:
    def __init__(self, name, loved_food, hated_food):
        self.name = name
        self.loved_food = loved_food
        self.hated_food = hated_food
    
    def taste(self, food):
        if food in self.loved_food:
            return 'Love it'
        elif food in self.hated_food:
            return 'Hate it'
        else:
            return 'Haven\'t tried it'


person = Person('Luiz', ['tortas', 'tacos', 'quesadillas'],
                ['champurrados', 'white beans', 'pho'])
print(person.taste('tacos'))
print(person.taste('pho'))
print(person.taste('thesenuts'))
