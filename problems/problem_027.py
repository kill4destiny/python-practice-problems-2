# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    #edgecase
    #if values empty:
    if len(values) == 0:
        #return None
        return None
    #variable max with the value of zero
    max = 0
    #for value in values:
    for value in values:
        #if max <= value:
        if max <= value:
            #max = value
            max = value
    #return max
    return max


print(max_in_list([20, 66, 32, 40, 1]))
print(max_in_list([15, 22, 33, 60]))
print(max_in_list([20, 10, 15]))
print(max_in_list([]))