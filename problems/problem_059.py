# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random

def specific_random():
    my_list = []
    while len(my_list) < 7:
        num = random.randint(10, 500)
        if num not in my_list and num % 5 == 0 and num % 7 == 0:
            my_list.append(num)
    return random.choice(my_list)


print(specific_random())
