# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    #edgeCase
    #if values in list is 1 or less:
    if len(values) <= 1:
        #return None
        return None
    #variable sorting with values with sorted method
    sorting = sorted(values)
    #return len sorting -2
    return sorting[len(sorting) -2]


print(find_second_largest([2, 5, 8, 4, 3, 1]))
print(find_second_largest([30, 56, 21, 32]))
print(find_second_largest([10, 3, 2, 30]))
