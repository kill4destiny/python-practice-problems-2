# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    #if age is greater or equal to 18 ,or has_consent_form equal true:
    if age >= 18 or has_consent_form:
        #return 'Your free to skydive, have fun!'
        return 'Your free to skydive, have fun!'
    #else
    else:
        #return 'You do not meet the qualifications to skydive!'
        return 'You do not meet the qualifications to skydive!'

print(can_skydive(18, False))
print(can_skydive(17, True))
print(can_skydive(20, True))
print(can_skydive(16, False))
